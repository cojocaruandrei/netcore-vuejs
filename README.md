## Setup
To build and run the project:

### Build and run the backend ASP.NET Core Web API application:
1. Restore nuget packages with `backend\AuthWebApi>dotnet restore` in the `backend\AuthWebApi` directory.
2. Create the database with `backend\AuthWebApi>dotnet ef database update` in the `backend\AuthWebApi` directory.
3. Run the project with `backend\AuthWebApi>dotnet run` in the `backend\AuthWebApi` directory.

### Build and run the frontend Vue.js application:
1. Install npm packages with `frontend>npm install` in the `frontend` directory.
2. Start the application with the node development serve `frontend>npm run dev` in the `frontend` directory.