using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace AuthWebApi.Data
{
  public class ApplicationUser : IdentityUser
  {
  }
}