import axios from 'axios';
import { Observable } from 'rxjs/Rx';

class AuthService {
    login(credentials){
alert('sas');

        return Observable.fromPromise(axios.post(`${this.api}/auth/login`, credentials))
        .map((res) => res.data.auth_token)
        .catch((error) => this.handleError(error.response));
    }
}

// export a singleton instance in the global namespace
export const authService = AuthService.Instance;
