import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/store'
import NotFoundComponent from '@/components/NotFoundComponent'
import Hello from '@/components/Hello'
import LoginForm from '@/views/account/LoginForm'
import RegisterForm from '@/views/account/RegisterForm'
import Dashboard from '@/views/dashboard/Home'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {path: '/', name: 'Hello', component: Hello},
    {path: '/login', name: 'loginForm', component: LoginForm},
    {path: '/register', name: 'registerForm', component: RegisterForm},
    {path: '/dashboard', name: 'dashboard', component: Dashboard, beforeEnter: requireAuth},
    { path: '*', component: NotFoundComponent }
  ]
})

function requireAuth (to, from, next) {
  if(store.getters['auth/isAuthenticated'])
    next()
  else
    next('login')
}
