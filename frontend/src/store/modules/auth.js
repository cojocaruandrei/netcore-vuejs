// import { Credentials } from '../../models/credentials.interface';
import { authService } from '../../services/auth.service';
import { Observable } from 'rxjs/Rx';
import axios from 'axios';
// import { EventBus } from '../../event-bus';

const state = { token: localStorage.getItem('auth-token') || '', status: '' };

const getters = {
    isAuthenticated: (authState) => !!authState.token,
    authStatus: (authState) => authState.status,
    authToken: (authState) => authState.token,
};

const actions = {
    authRequest: ({commit, dispatch} , credentials) => {
        return new Promise((resolve, reject) => {
            commit('authRequest');
            axios.post(process.env.API_URL + '/Auth/login', 
            {
                "userName": credentials.userName,
                "password": credentials.password
            },
            {
                headers: {"Content-Type": 'application/json'},
            }
            ).then(function(response){
                localStorage.setItem('auth-token', response.data.auth_token); // stash the auth token in localStorage
                commit('authSuccess', response.data.auth_token);
                // EventBus.$emit('logged-in', null);
                dispatch('user/userRequest', null, { root: true });
                resolve(response.data.auth_token);
            }).catch(function(error){
                console.log(error)
                commit('authError', error);
                localStorage.removeItem('auth-token');
                reject(error);
            });
        });
    },
    authLogout: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {
          commit('authLogout');
          localStorage.removeItem('auth-token');
          resolve();
        });
    },
};

const mutations = {
    authRequest: (authState) => {
        authState.status = 'attempting authentication request';
    },
    authSuccess: (authState, authToken) => {
        authState.status = 'authentication succeeded';
        authState.token = authToken;
    },
    authError: (authState) => {
        authState.status = 'error';
    },
    authLogout: (authState) => {
        authState.token = '';
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};

