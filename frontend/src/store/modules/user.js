// import { profileService } from '../../services/profile.service';
// import { EventBus } from '../../event-bus';
import Vue from 'vue';
import axios from 'axios';

const state = { profile: {}, status: '' };

const getters = {
    profile: (userState) => userState.profile,
};

const actions = {
    userRequest: ({commit, dispatch, rootState}) => {
        commit('userRequest');
        const AuthStr = 'Bearer '.concat(rootState.auth.token);

        axios.get(process.env.API_URL + '/profile/me', { headers: { Authorization: AuthStr } })
            .then(function(response){
                commit('userSuccess', response.data);
            }).catch(function(error){
                console.log(error)
                commit('userError');
                dispatch('auth/authLogout', null, { root: true });
            });
    },
};

const mutations = {
    userRequest: (userState) => {
        userState.status = 'attempting request for user profile data';
    },
    userSuccess: (userState, userResp) => {
      userState.status = 'success';
      Vue.set(userState, 'profile', userResp);
    },
    userError: (userState) => {
      userState.status = 'error';
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};

